﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour {

    [SerializeField] private GameObject objectPool;
    [SerializeField] private GameObject panArea;

    public Vector3 position;
    public bool isPress, isDrag;
    private static InputManager instance;
    private Collider2D currentItem;
    private Vector3 originalPos;
    private float time = 0;

    // Create Instance of InputManager
    public static InputManager Instance {
        get {
            if (instance == null)
                instance = GameObject.FindObjectOfType<InputManager>();
            return instance;
        }
    }

    void Start() {
        //Initialize Input
        position = new Vector3(0, 0, 0);
        isPress = false;
        isDrag = false;
    }

    void Update() {
        //Get Position and Press State
#if !UNITY_EDITOR
		var t = Input.touches;
		if(t.Length<=0){
			isPress = false;
		}
		if(t.Length>0){
			position.x = t[t.Length-1].position.x;
			position.y = t[t.Length-1].position.y;
			position.z = 0;
		    position = new Vector3(Camera.main.ScreenToWorldPoint(position).x, Camera.main.ScreenToWorldPoint(position).y, 0);
            isPress = true;
        }
#else
        var t = Input.mousePosition;
        position.x = t.x;
        position.y = t.y;
        position.z = 0;
        position = new Vector3(Camera.main.ScreenToWorldPoint(position).x, Camera.main.ScreenToWorldPoint(position).y, 0);
        isPress = Input.GetMouseButton(0);
#endif
        //When Press...
        if (isPress) {
            //If not still draging, find the block at the pointer location
            if (!isDrag) FindBlock();
            //If there is any block, start draging
            if (currentItem != null) DragBlock(currentItem);
        }
        //When Release...
        else {
            //If still dragging...
            if (isDrag && currentItem != null) {
                //Calculate current speed and angle of draging item
                var speed = MoveDistance(originalPos, currentItem.transform.position) / (Time.time - time);
                var angle = MoveAngle(originalPos, currentItem.transform.position);
                //Set Speed of draging item
                currentItem.GetComponent<Rigidbody2D>().velocity = new Vector2(speed * Mathf.Cos(angle * Mathf.Deg2Rad), speed * Mathf.Sin(angle * Mathf.Deg2Rad));
                //Assist Velocity
                if (currentItem.gameObject.tag == "Ingredient") {
                    currentItem.GetComponent<Rigidbody2D>().velocity += (Vector2)(panArea.transform.position - currentItem.transform.position) * 2;
                }
                //Auto destroy item when it stop
                StartCoroutine(EndDrag(currentItem.gameObject));
            }
            //Stop Draging
            isDrag = false;
        }
    }

    //Auto destroy item when it stop
    public IEnumerator EndDrag(GameObject obj) {
        //Wait for item to stop 
        while (obj != null && obj.GetComponent<Rigidbody2D>().velocity.magnitude > 10f)
            yield return new WaitForSeconds(0.1f);
        //Fade out item
        while (obj != null && obj.GetComponent<Image>().color.a > 0) {
            Color c = obj.GetComponent<Image>().color;
            c.a -= 0.1f;
            obj.GetComponent<Image>().color = c;
            yield return new WaitForSeconds(0.05f);
        }
        //Destroy object
        if (obj != null) Destroy(obj);
        yield return 0;
    }

    //Find the block at the pointer location
    private void FindBlock() {
        //Reset current item
        currentItem = null;
        //Get all item in dragging pool
        foreach (Collider2D item in objectPool.GetComponentsInChildren<Collider2D>()) {
            if (item.gameObject.tag != "Ingredient" && item.gameObject.tag != "Crepe") continue;
            //Check if any item is at the pointer location
            var normalizedPosition = new Vector3(position.x, position.y, item.transform.position.z);
            if (item.bounds.Contains(normalizedPosition)) {
                currentItem = item;
                break;
            }
        }
    }

    //Drag Block
    private void DragBlock(Collider2D currentItem) {
        //Set Block Position to Pointer Position
        currentItem.transform.position = position;
        currentItem.transform.localPosition = new Vector3(currentItem.transform.localPosition.x, currentItem.transform.localPosition.y, 0f);
        //Save the last pos and time for calculating speed and direction
        if (!isDrag) {
            if (!isDrag) currentItem.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            originalPos = currentItem.transform.position;
            time = Time.time;
        }
        //Set drag state to true
        isDrag = true;
    }

    //Calculate Angle between 2 point
    private float MoveAngle(Vector3 before, Vector3 after) {
        float angle = Mathf.Atan2(after.y - before.y, after.x - before.x) * Mathf.Rad2Deg;
        if (angle < 0) angle += 360;
        return angle;
    }

    //Calculate distance between 2 point
    private float MoveDistance(Vector3 before, Vector3 after) {
        float distance = Mathf.Sqrt((after.y - before.y) * (after.y - before.y) + (after.x - before.x) * (after.x - before.x));
        return distance;
    }
}
