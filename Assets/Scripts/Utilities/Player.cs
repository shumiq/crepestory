﻿using UnityEngine;
using System.Collections;
using System;

public static class Player {
    
	public static int GetLife() {
        int currentLife = PlayerPrefs.GetInt("Life", GameConfig.MAX_LIFE);
        int lastRegen = PlayerPrefs.GetInt("LastRegen", now() - GameConfig.REGEN_TIME);
        if(now() - lastRegen >= GameConfig.REGEN_TIME) {
            currentLife = Math.Min(GameConfig.MAX_LIFE, currentLife + (int)((now() - lastRegen)/ GameConfig.REGEN_TIME));
            SetLife(currentLife);
            PlayerPrefs.SetInt("LastRegen", now());
        }
        return currentLife;
    }

    private static int now() {
        return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
    }

    public static void SetLife(int life) {
        PlayerPrefs.SetInt("Life", life);
    }
    
    public static void IncreaseLife(int n = 1) {
        SetLife(Math.Min(GameConfig.MAX_LIFE, GetLife() + n));
    }
    
    public static void DecreaseLife(int n = 1) {
        SetLife(Math.Max(0, GetLife() - n));
    }

    public static bool Playable() {
        return GetLife() != 0;
    }
    
    public static int GetHighScore() {
        return PlayerPrefs.GetInt("HighScore", 0);
    }

    public static void SetHighScore(int score) {
        if (score > GetHighScore())
            PlayerPrefs.SetInt("HighScore", score);
    }
}
