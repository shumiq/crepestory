﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Game {

    public static float RemainTime;
    public static int Score;
    public static float ScoreFactor;
    public static float ComboFactor;
    public static int ComboCount;
    public static bool Pause;
    public static Dictionary<string, Sprite> IngredientsSpriteNormal;
    public static Dictionary<string, Sprite> IngredientsSpritePan;
    public static Dictionary<string, Sprite> IngredientsSpriteFinish;

    // WaitForSeconds with Pause
    public static IEnumerator WaitWithPause(float seconds) {
        float timeCount = 0f;
        while (timeCount < seconds) {
            if (!Pause)
                timeCount += Time.deltaTime;
            yield return null;
        }
    }
}
