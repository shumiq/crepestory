using UnityEngine.Advertisements;
using UnityEngine;
using System;

public class VideoAdsManager : MonoBehaviour {
    private static Action finishCallback;
    public static void ShowVideo(Action callback = null) {
        if (Advertisement.IsReady("rewardedVideo")) {
            finishCallback = callback;
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }
    private static void HandleShowResult(ShowResult result) {
        switch (result) {
            case ShowResult.Finished:
                break;
            case ShowResult.Skipped:
                break;
            case ShowResult.Failed:
                break;
        }
        if (finishCallback != null) finishCallback();
    }
}
