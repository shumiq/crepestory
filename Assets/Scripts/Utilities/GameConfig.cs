﻿using UnityEngine;
using System.Collections;

public static class GameConfig {
    
    // Life
    public static readonly int REGEN_TIME = 3600;
    public static readonly int MAX_LIFE = 3;

    // Gauge & Score
    public static readonly float MAX_TIME = 90f;
    public static readonly float SCORE_THRESHOLD = 3000;
    public static readonly float COMBO_THRESHOLD = 20;
    public static readonly float MAX_SCORE_FACTOR = 3f;
    public static readonly float MAX_COMBO_FACTOR = 2f;

    // Game
    public static readonly float WAIT_BEFORE_ORDER_MIN = 1f;
    public static readonly float WAIT_BEFORE_ORDER_MAX = 4f;
    public static readonly int NUMBER_INGREDIENT_MIN = 1;
    public static readonly int NUMBER_INGREDIENT_MAX = 3;
    public static readonly float ORDER_WAIT_MIN = 4f;
    public static readonly float ORDER_WAIT_MAX = 8f;
    public static readonly int MAX_ORDER = 6;
    public static readonly float SHOW_LABEL_TIME = 2f;
}
