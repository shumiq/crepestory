﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class PausePopup : MonoBehaviour {

    [SerializeField] private Button inputBlocker;
    [SerializeField] private Button resetButton;
    [SerializeField] private Button videoButton;
    [SerializeField] private Image[] lifes;

    private int lifeCount;

    private void Start() {
        // Add InputBlocker Event
        inputBlocker.onClick.AddListener(() => {
            Hide(() => CrepeShopManager.Instance.Pause(false));
        });
        // Add Reset Event
        resetButton.onClick.AddListener(() => {
            Player.DecreaseLife();
            SceneManager.LoadScene("CrepeShop");
        });
        // Add VideoAds Event
        videoButton.onClick.AddListener(() => {
            VideoAdsManager.ShowVideo(() => {
                Player.IncreaseLife();
                updateLife();
            });
        });
    }

    public void Init() {
        updateLife();
    }

    public void Show(Action callback = null) {
        inputBlocker.gameObject.SetActive(true);
        playAnimation("PopupInTransition", callback);
    }

    public void Hide(Action callback = null) {
        inputBlocker.gameObject.SetActive(false);
        playAnimation("PopupOutTransition", callback);
    }
    
    private void playAnimation(string animationName, Action callback = null) {
        StartCoroutine(playAnimationCoroutine(animationName, callback));
    }

    private IEnumerator playAnimationCoroutine(string animationName, Action callback) {
        Animator anim = this.GetComponent<Animator>();
        anim.Play(animationName);
        while (!anim.GetCurrentAnimatorStateInfo(0).IsName(animationName))
            yield return new WaitForSeconds(0.3f);
        if (callback != null)
            callback();
        yield return 0;
    }

    private void updateLife() {
        resetButton.interactable = Player.Playable();
        for (int i = 0; i < Player.GetLife(); i++) {
            lifes[i].color = Color.white;
        }
    }
}
