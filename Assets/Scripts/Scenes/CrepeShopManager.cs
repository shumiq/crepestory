﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CrepeShopManager : MonoBehaviour {

    //Top
    [SerializeField] private Gauge gauge;
    //GameOver
    [SerializeField] private GameOverPopup gameOverPopup;
    //Pause
    [SerializeField] private Button pauseButton;
    [SerializeField] private PausePopup pausePopup;
    [SerializeField] private string[] pauseString;
    [SerializeField] private Text pauseLabel;
    //Customer
    [SerializeField] private CustomerManager customers;
    //Ingredient
    [SerializeField] private IngredientManager mainIngredient;
    [SerializeField] private IngredientManager subIngredient;
    //Ingredient
    [SerializeField] private PanManager pan;
    //Misc
    [SerializeField] public GameObject DragPool;

    private static CrepeShopManager instance;
    // Create Instance of CrepeShopManager
    public static CrepeShopManager Instance {
        get {
            if (instance == null)
                instance = GameObject.FindObjectOfType<CrepeShopManager>();
            return instance;
        }
    }

    public bool isStart { get; private set; }

    void Start () {
        Init();
	}

    private void Init() {
        pauseInit();
        gameVariableInit();
        gauge.Init();
        customers.Init();
        mainIngredient.Init();
        subIngredient.Init();
        pan.Init();
        isStart = true;
    }

    private void gameVariableInit() {
        Game.RemainTime = GameConfig.MAX_TIME;
        Game.Score = 0;
        Game.ComboCount = 0;
        Game.ScoreFactor = 1;
        Game.ComboFactor = 1;
        Game.Pause = false;
    }

    private void pauseInit() {
        pauseLabel.text = pauseString[0];
        pauseButton.onClick.AddListener(() => {
            Pause(!Game.Pause);
        });
    }
	
    public void GameOver() {
        if (Game.Pause) return;
        Game.Pause = true;
        gameOverPopup.Init();
        gameOverPopup.Show();
    }

    public void Pause(bool p = true) {
        if (p) {
            pauseLabel.text = pauseString[1];
            Game.Pause = true;
            pausePopup.Init();
            pausePopup.Show();
        }
        else {
            pausePopup.Hide(() => {
                pauseLabel.text = pauseString[0];
                Game.Pause = false;
            });
        }
    }

    public void AddScore(int score) {
        gauge.AddScore(score);
    }
}
