﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleManager : MonoBehaviour {

    [SerializeField] private Button startButton;
    [SerializeField] private Button videoButton;
    [SerializeField] private Image[] lifes;
    [SerializeField] private Text highScore;


    void Start () {
        // Add Start Event
        startButton.onClick.AddListener(() => {
            Player.DecreaseLife();
            SceneManager.LoadScene("CrepeShop");
        });
        // Add VideoAds Event
        videoButton.onClick.AddListener(() => {
            VideoAdsManager.ShowVideo(() => {
                Player.IncreaseLife();
                updateLife();
            });
        });
        // Update Life & High Score
        updateLife();
        updateHighScore();
    }

    private void updateLife() {
        startButton.interactable = Player.Playable();
        for (int i = 0; i < Player.GetLife(); i++) {
            lifes[i].color = Color.white;
        }
    }

    private void updateHighScore() {
        highScore.text = "HighScore: " + Player.GetHighScore();
    }
}
