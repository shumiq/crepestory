﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Gauge : MonoBehaviour {

    [SerializeField] private Image gauge;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text comboText;

    private static Gauge instance;

    // Create Instance of Gauge
    public static Gauge Instance {
        get {
            if (instance == null)
                instance = GameObject.FindObjectOfType<Gauge>();
            return instance;
        }
    }

    public void Init () {
        scoreText.text = "0";
    }

    private void Update() {
        if (!CrepeShopManager.Instance.isStart) return;
        // Prevent life overflow
        Game.RemainTime = Math.Max(0, Math.Min(Game.RemainTime, GameConfig.MAX_TIME));
        // Resize Gauge
        gauge.transform.localScale = new Vector3(1f* Game.RemainTime / GameConfig.MAX_TIME, 1, 1);
        // If timeup, game over
        if (Game.RemainTime <= 0) CrepeShopManager.Instance.GameOver();
        // Reduce Life if game not pause
        if(!Game.Pause) Game.RemainTime -= Time.deltaTime;
    }

    public void AddScore(int score) {
        // Set Score
        Game.Score += score;
        scoreText.text = "" + Game.Score;
        // Set Combo
        if (score > 0) {
            Game.ComboCount++;
            comboText.gameObject.SetActive(true);
            comboText.text = "Combo: " + Game.ComboCount;
        }
        else {
            Game.ComboCount = 0;
            comboText.gameObject.SetActive(false);
        }
        // Update Score & Combo Factor
        Game.ComboFactor = 1f + (GameConfig.MAX_COMBO_FACTOR -1) * ((float)Game.ComboCount / GameConfig.COMBO_THRESHOLD);
        Game.ScoreFactor = 1f + (GameConfig.MAX_SCORE_FACTOR - 1) * (float)Game.Score / GameConfig.SCORE_THRESHOLD;
    }
}
