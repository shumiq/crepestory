﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class IngredientManager : MonoBehaviour {

    [SerializeField] private string[] names;
    [SerializeField] private Button[] ingredientsButton;
    [SerializeField] private Sprite[] ingredientsNormal;
    [SerializeField] private Sprite[] ingredientsPan;
    [SerializeField] private Sprite[] ingredientsFinish;

    public void Init() {
        //Initialize IngredientsMap if not
        if (Game.IngredientsSpriteNormal == null) Game.IngredientsSpriteNormal = new Dictionary<string, Sprite>();
        if (Game.IngredientsSpritePan == null) Game.IngredientsSpritePan = new Dictionary<string, Sprite>();
        if (Game.IngredientsSpriteFinish == null) Game.IngredientsSpriteFinish = new Dictionary<string, Sprite>();
        for (int i = 0; i < ingredientsButton.Length; i++) {
            // Get Currrent Name and Button
            var name = names[i];
            var button = ingredientsButton[i];
            // Add Ingredient to IngredientMap
            if(!Game.IngredientsSpriteNormal.ContainsKey(name))
                Game.IngredientsSpriteNormal.Add(name, ingredientsNormal[i]);
            if (!Game.IngredientsSpritePan.ContainsKey(name))
                Game.IngredientsSpritePan.Add(name, ingredientsPan[i]);
            if (!Game.IngredientsSpriteFinish.ContainsKey(name))
                Game.IngredientsSpriteFinish.Add(name, ingredientsFinish[i]);
            // Set Button Sprite
            button.GetComponent<Image>().sprite = Game.IngredientsSpriteNormal[name];
            // Create pointerDown Event Listener to button
            EventTrigger trigger = button.gameObject.AddComponent<EventTrigger>();
            var pointerDown = new EventTrigger.Entry();
            pointerDown.eventID = EventTriggerType.PointerDown;
            pointerDown.callback.AddListener((e) => {
                //Create the Ingredient Instance when pointerDown
                GameObject ingredientInstance = (GameObject)Instantiate(Resources.Load("IngredientInstance"));
                //Set instance name as Ingredient name
                ingredientInstance.name = name + "Instance";
                //Set instance image
                var img = ingredientInstance.GetComponent<Image>();
                img.sprite = button.GetComponent<Image>().sprite;
                //Set position, parent, scale
                ingredientInstance.transform.position = button.transform.position;
                ingredientInstance.transform.SetParent(CrepeShopManager.Instance.DragPool.transform);
                ingredientInstance.transform.localScale = Vector3.one;
            });
            // Add trigger to button
            trigger.triggers.Add(pointerDown);
        }
    }
}