﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class Customer : MonoBehaviour {

    [SerializeField] GameObject label;
    [SerializeField] Image customer;
    [SerializeField] Sprite[] customerImageList;
    [SerializeField] Sprite[] satisfyImage;
    [SerializeField] Text scoreText;

    private Dictionary<string, int> orderList;
    public bool isWait { get; private set; }
    private float score;
    private float waitTimeCount;

    public void SetOrder(List<string> order, float waitTime) {
        // Create order dictionary
        orderList = new Dictionary<string, int>();
        // Set Score
        this.score = waitTime;
        // Set order data from list to dictionary
        foreach (string ing in order) {
            if (orderList.ContainsKey(ing)) orderList[ing]++;
            else orderList.Add(ing, 1);
        }
        ShowImage(order);
        // Start Waiting
        StartCoroutine(waitOrder(waitTime));
    }

    // Wait coroutine
    private IEnumerator waitOrder(float waitTime) {
        isWait = true;
        waitTimeCount = 0f;
        while (waitTimeCount < waitTime && isWait) {
            if (!Game.Pause)
                waitTimeCount += Time.deltaTime;
            yield return null;
        }
        //If timeout, not satisfy
        if(isWait)GetCrepe(false, 0);
    }

    void OnCollisionEnter2D(Collision2D collision) {
        // If not Crepe or customer not wait, do nothing
        if (collision.gameObject.tag != "Crepe" || !isWait) return;

        // Get Crepe Component
        var Crepe = collision.gameObject.GetComponent<Crepe>();
        // Check Crepe same or not
        GetCrepe(Crepe.isSame(orderList));

        // Destroy Crepe
        if(Crepe.isSame(orderList))
            Destroy(collision.gameObject);
    }

    public void GetCrepe(bool satisfy) {
        GetCrepe(satisfy, score);
    }

    public void GetCrepe(bool satisfy, float score) {
        // If Customer not wait, do nothing
        if (!isWait) return;
        // Customer stop waiting
        isWait = false;
        // Check if customer satisfy
        if (satisfy) {
            ShowLabel(true);
            Game.RemainTime += score;
            int s = (int)(((GameConfig.ORDER_WAIT_MAX - score) - waitTimeCount) * 10 * Game.ScoreFactor);
            s = (int)(Math.Max(10, s) * (1 + (Game.ComboCount/2)));
            ShowScore(s);
            CrepeShopManager.Instance.AddScore(s);
        }
        else {
            ShowLabel(false);
            Game.RemainTime -= score;
            CrepeShopManager.Instance.AddScore(0);
        }
    }

    // Show Bubble Image
    public void ShowImage(List<string> orders) {
        if (orders == null || orders.Count == 0) {
            label.SetActive(false);
        }
        else {
            label.SetActive(true);
            var layout = label.GetComponentInChildren<GridLayoutGroup>();
            for (int i = layout.transform.childCount - 1; i >= 0; i--)
                Destroy(layout.transform.GetChild(i).gameObject);
            foreach (string order in orders) {
                GameObject bubbleInstance = (GameObject)Instantiate(Resources.Load("IngredientBubble"));
                bubbleInstance.GetComponent<Image>().sprite = Game.IngredientsSpriteNormal[order];
                bubbleInstance.transform.SetParent(layout.transform);
            }
        }
    }
    public void ShowImage(bool isSatisfy) {
        label.SetActive(true);
        var layout = label.GetComponentInChildren<GridLayoutGroup>();
        for(int i = layout.transform.childCount-1; i>=0; i--)
            Destroy(layout.transform.GetChild(i).gameObject);
        if (isSatisfy) {
            GameObject bubbleInstance = (GameObject)Instantiate(Resources.Load("IngredientBubble"));
            bubbleInstance.GetComponent<Image>().sprite = satisfyImage[0];
            bubbleInstance.transform.SetParent(layout.transform);
        }
        else {
            GameObject bubbleInstance = (GameObject)Instantiate(Resources.Load("IngredientBubble"));
            bubbleInstance.GetComponent<Image>().sprite = satisfyImage[1];
            bubbleInstance.transform.SetParent(layout.transform);
        }
    }

    // Show Bubble + Autohide
    public void ShowLabel(bool isSatisfy, Action callback = null) {
        StartCoroutine(ShowLabelCoroutine(isSatisfy, callback));
    }
    private IEnumerator ShowLabelCoroutine(bool isSatisfy, Action callback) {
        ShowImage(isSatisfy);
        yield return Game.WaitWithPause(GameConfig.SHOW_LABEL_TIME);
        ShowImage(null);
    }

    // Show Score and auto hide
    public void ShowScore(int score, Action callback = null) {
        StartCoroutine(ShowScoreCoroutine(score, callback));
    }
    private IEnumerator ShowScoreCoroutine(int score, Action callback) {
        scoreText.text = "+" + score;
        yield return Game.WaitWithPause(GameConfig.SHOW_LABEL_TIME);
        scoreText.text = "";
    }

    // Set Customer Active
    public void SetCustomerActive(bool active) {
        if (active) {
            customer.sprite = customerImageList[UnityEngine.Random.Range(0, customerImageList.Length - 1)];
        }
        customer.gameObject.SetActive(active);
    }
}
