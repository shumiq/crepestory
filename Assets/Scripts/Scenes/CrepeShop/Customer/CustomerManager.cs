﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System;

public class CustomerManager : MonoBehaviour {

    [SerializeField] Customer[] customers;
    
    public void Init() {
        // Start Making order for each customers
        foreach (Customer c in customers) {
            StartCoroutine(startOrder(c));
        }
    }

    private IEnumerator startOrder(Customer customer) {
        while (true) {
            // Wait before order
            float time = UnityEngine.Random.Range(
                GameConfig.WAIT_BEFORE_ORDER_MIN, GameConfig.WAIT_BEFORE_ORDER_MAX)
                /(Game.ScoreFactor * Game.ComboFactor);
            yield return Game.WaitWithPause(time);
            customer.SetCustomerActive(true);
            yield return Game.WaitWithPause(1f);
            // Random Order
            List<string> order = randomOrder();
            customer.SetOrder(order, UnityEngine.Random.Range(
                GameConfig.ORDER_WAIT_MIN, GameConfig.ORDER_WAIT_MAX)
                /(Game.ScoreFactor * Game.ComboFactor) 
                + order.Count/2);
            // Wait for order receive
            while (customer.isWait)
                yield return Game.WaitWithPause(0.1f);
            // Wait for satisfy
            yield return Game.WaitWithPause(GameConfig.SHOW_LABEL_TIME);
            // Customer Leave
            yield return Game.WaitWithPause(1f);
            customer.ShowImage(null);
            customer.SetCustomerActive(false);
        }
    }

    private List<string> randomOrder() {
        // Random number of order
        int numOrder = UnityEngine.Random.Range(
            GameConfig.NUMBER_INGREDIENT_MIN, GameConfig.NUMBER_INGREDIENT_MAX) 
            * (int)(Game.ScoreFactor * Game.ComboFactor);
        numOrder = Math.Min(numOrder, GameConfig.MAX_ORDER);
        return randomOrder(numOrder);
    }

    private List<string> randomOrder(int numOrder) {
        // Create Order
        List<string> order = new List<string>();
        // Get Ingredients List into array
        string[] ing = Game.IngredientsSpriteNormal.Keys.ToArray();
        // Shuffle Array
        for (int i = 0; i < ing.Length; i++) {
            int rnd = UnityEngine.Random.Range(0, ing.Length);
            var temp = ing[rnd];
            ing[rnd] = ing[i];
            ing[i] = temp;
        }
        // Random Ingredient and add to order
        for (int i = 0; i < numOrder; i++)
            order.Add(ing[i]);
        return order;
    }
}
