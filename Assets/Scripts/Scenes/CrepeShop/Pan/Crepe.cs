﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class Crepe : MonoBehaviour {
    
    private Dictionary<string, int> ingredientList;

    public void addIngredient(string ingredient) {
        if (ingredientList == null) ingredientList = new Dictionary<string, int>();

        //If exist ingredient, increase it, if not, add new
        if (ingredientList.ContainsKey(ingredient)) ingredientList[ingredient]++;
        else ingredientList.Add(ingredient, 1);

        //Add Ingredient
        GameObject ingredientFinishInstance = (GameObject)Instantiate(Resources.Load("IngredientFinish"));
        ingredientFinishInstance.GetComponent<Image>().sprite = Game.IngredientsSpriteFinish[ingredient];
        ingredientFinishInstance.transform.SetParent(this.transform);
        ingredientFinishInstance.transform.localPosition = Vector3.zero;
        ingredientFinishInstance.transform.localScale = Vector3.one;
    }

    public bool isSame(Dictionary<string, int> order) {
        //Check if all ingredient in current Crepe exist and same amount in order
        foreach (string key in ingredientList.Keys) {
            int value = 0;
            if (!order.TryGetValue(key, out value) || ingredientList[key] != value)
                return false;
        }
        //Check if all ingredient in order exist and same amount in current Crepe
        foreach (string key in order.Keys) {
            int value = 0;
            if (!ingredientList.TryGetValue(key, out value) || order[key] != value)
                return false;
        }
        return true;
    }

}
