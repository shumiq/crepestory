﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class PanManager : MonoBehaviour {

    [SerializeField] private GameObject ingredientPool;
    [SerializeField] private Button crepe;
    [SerializeField] private Button reset;
    private List<string> ingredientList;

    public void Init() {
        //Initialize
        ingredientList = new List<string>();

        // Add Reset Event
        reset.onClick.AddListener(() => {
            clearPan();
        });

        // Create pointerDown Event Listener to Crepe Button
        EventTrigger trigger = crepe.gameObject.AddComponent<EventTrigger>();
        var pointerDown = new EventTrigger.Entry();
        pointerDown.eventID = EventTriggerType.PointerDown;
        pointerDown.callback.AddListener((e) => {
            // If there is no ingredient, do nothing
            if (ingredientList.Count == 0) return;
            //Create the Crepe Instance when pointerDown
            GameObject CrepeInstance = (GameObject)Instantiate(Resources.Load("CrepeInstance"));
            //Set instance name as Ingredient name
            CrepeInstance.name = "CrepeInstance";
            //Get Crepe Component
            Crepe CrepeComponent = CrepeInstance.GetComponent<Crepe>();
            //Add Ingredient
            foreach (string ingredient in ingredientList)
                CrepeComponent.addIngredient(ingredient);
            //Set position, parent, scale
            CrepeInstance.transform.position = crepe.transform.position;
            CrepeInstance.transform.SetParent(CrepeShopManager.Instance.DragPool.transform);
            CrepeInstance.transform.localScale = Vector3.one;
            //Reset current Crepe on the pan
            clearPan();
        });
        // Add trigger to button
        trigger.triggers.Add(pointerDown);
    }

    void OnCollisionEnter2D(Collision2D collision) {
        // If not ingredient, do nothing
        if (collision.gameObject.tag != "Ingredient") return;
        // If exist ingredient, do nothing
        if (ingredientList.Exists(s => s == collision.gameObject.name.Replace("Instance", ""))) return;
        // Create Crepebase
        crepe.gameObject.SetActive(true);
        // Add Ingredient Text to Crepe
        string name = collision.gameObject.name.Replace("Instance", "");
        GameObject ingredientPanInstance = (GameObject)Instantiate(Resources.Load("IngredientPan"));
        ingredientPanInstance.GetComponent<Image>().sprite = Game.IngredientsSpritePan[name];
        ingredientPanInstance.transform.SetParent(ingredientPool.transform);
        ingredientPanInstance.transform.localPosition = Vector3.zero;
        ingredientPanInstance.transform.localScale = Vector3.one;
        // Add Ingredient to list
        ingredientList.Add(collision.gameObject.name.Replace("Instance", ""));
        // Destroy Ingredient Object
        Destroy(collision.gameObject);
    }

    private void clearPan() {
        for (int i = ingredientPool.transform.childCount - 1; i >= 0; i--)
            Destroy(ingredientPool.transform.GetChild(i).gameObject);
        ingredientList.Clear();
        crepe.gameObject.SetActive(false);
    }
}
